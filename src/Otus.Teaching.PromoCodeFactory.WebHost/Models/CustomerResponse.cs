﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public CustomerResponse() { }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Preferences = customer.CustomerPreferences.Select(pr => pr.Preference.Name).ToList();
            PromoCodes = customer.PromoCodes.Select(pr => new PromoCodeShortResponse
            {
                Id = pr.Id,
                BeginDate = pr.BeginDate.ToString("dd.MM.yyyy"),
                EndDate = pr.EndDate.ToString("dd.MM.yyyy"),
                Code = pr.Code,
                PartnerName = pr.PartnerName,
                ServiceInfo = pr.ServiceInfo
            }).ToList();

        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public List<string> Preferences { get; set; }

        public List<PromoCodeShortResponse> PromoCodes { get; set; }

    }
}