﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly ICustomersRepository _customersRepository;

        public CustomersController(ICustomersRepository customersRepository)
        {
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var result = await _customersRepository.GetAllAsync();

            return result.Select(customer => new CustomerShortResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            });

        }

        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <returns></returns>

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {

            var customer = await _customersRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return Ok(response);
        }


        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = request.PreferenceIds.Select(prId => new CustomerPreference { PreferenceId = prId }).ToList()
            };

            await _customersRepository.AddAsync(customer);

            // return Ok(new { id = customer.Id });
            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id);
        }

        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.CustomerPreferences = request.PreferenceIds.Select(prId => new CustomerPreference { PreferenceId = prId }).ToList();

            await _customersRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента со связанными сущностями (препочтения/промокоды)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            await _customersRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}