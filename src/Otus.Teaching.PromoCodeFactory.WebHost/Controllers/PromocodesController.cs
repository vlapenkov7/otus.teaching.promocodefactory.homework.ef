﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<PromoCode> _promocodesRepository;
        private readonly IRepository<Employee> _employeesRepository;
        private readonly ICustomersRepository _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;



        public PromocodesController(IRepository<PromoCode> promocodesRepository, IRepository<Employee> employeesRepository, ICustomersRepository customersRepository, IRepository<Preference> preferenceRepository)
        {
            _promocodesRepository = promocodesRepository;
            _employeesRepository = employeesRepository;
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            IEnumerable<PromoCode> promoCodes = await _promocodesRepository.GetAllAsync();

            return promoCodes.Select(self => new PromoCodeShortResponse
            {
                BeginDate = self.BeginDate.ToString("dd.MM.yyyy"),
                EndDate = self.EndDate.ToString("dd.MM.yyyy"),
                Code = self.Code,
                Id = self.Id,
                PartnerName = self.PartnerName,
                ServiceInfo = self.ServiceInfo
            });
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO:  функционал в слой логики

            // 1. находим партнера (Employee) по PartnerName

            var employeeFound = await _employeesRepository
                .GetFirstWhere(employee => employee.LastName == request.PartnerName)
                ?? throw new EntityNotFoundException($"Не найден партнер по имени {request.PartnerName}");


            // 2. находим клиентов с preferecnce = Preference

            var customersFound = await _customersRepository.GetCustomersByPreference(request.Preference);


            // 3. находим id с preferecnce = Preference
            var preferenceFound = await _preferenceRepository.GetFirstWhere(p => p.Name == request.Preference)
            ?? throw new EntityNotFoundException($"Не найдено предпочтение {request.Preference}");

            // 4. Генерируем промокод

            foreach (var customerFound in customersFound)
            {
                var promocodeFound = new PromoCode
                {
                    Id = Guid.NewGuid(),
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerManagerId = employeeFound.Id,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddYears(1),
                    PartnerName = employeeFound.FullName,
                    CustomerId = customerFound.Id,
                    PreferenceId = preferenceFound.Id
                };
                await _promocodesRepository.AddAsync(promocodeFound);
            }

            return Ok();
        }
    }
}