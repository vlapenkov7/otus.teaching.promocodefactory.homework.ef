﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Кросс сущность для соответствия Customer и Preference
    /// </summary>
    public class CustomerPreference
    {
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid PreferenceId { get; set; }

        public virtual Preference Preference { get; set; }
    }
}
