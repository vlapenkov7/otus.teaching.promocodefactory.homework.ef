﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomersRepository : IRepository<Customer>
    {
        Task<IEnumerable<Customer>> GetCustomersByPreference(string preference);

    }
}
