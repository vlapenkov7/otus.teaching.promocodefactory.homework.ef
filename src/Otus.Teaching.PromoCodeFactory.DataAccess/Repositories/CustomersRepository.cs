﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Дополнительный репозиторий для загрузки связанных сущностей
    /// </summary>
    public class CustomersRepository : EfRepository<Customer>, ICustomersRepository
    {

        public CustomersRepository(AppDbContext dataContext) : base(dataContext) { }

        /// <summary>
        /// Используем eager loading для подгрузки связанных сущностей для Customer
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <returns></returns>
        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext.Set<Customer>()
                .Include(cust => cust.CustomerPreferences)
                    .ThenInclude(cp => cp.Preference)
                .Include(cust => cust.PromoCodes)
                .FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }

        /// <summary>
        /// Получить клиентов по предпочтению
        /// </summary>
        /// <param name="preference">предпочтение</param>
        /// <returns></returns>
        public async Task<IEnumerable<Customer>> GetCustomersByPreference(string preference)
        {
            return await _dataContext.Set<Customer>()
                     .Include(cust => cust.CustomerPreferences)
                         .ThenInclude(cp => cp.Preference)
                         .Where(cust => cust.CustomerPreferences.Any(cp => cp.Preference.Name == preference))
                         .AsNoTracking()
                         .ToListAsync();
        }

    }
}
