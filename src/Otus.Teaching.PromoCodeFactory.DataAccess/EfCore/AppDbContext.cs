﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity
                .HasOne(employee => employee.Role)
                .WithMany()
                .HasForeignKey(employee => employee.RoleId);
            });

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.HasOne(promocode => promocode.PartnerManager)
                .WithMany()
                .HasForeignKey(employee => employee.PartnerManagerId);

                entity.HasOne(promocode => promocode.Preference)
               .WithMany()
               .HasForeignKey(preference => preference.PreferenceId);

            });


            modelBuilder.Entity<CustomerPreference>
            (entity =>
            {
                entity.HasKey(entity => new { entity.CustomerId, entity.PreferenceId });

                entity.HasOne(entity => entity.Preference)
                .WithMany()
                .HasForeignKey(entity => entity.PreferenceId);

                entity.HasOne(entity => entity.Customer)
                 .WithMany(customer => customer.CustomerPreferences)
                 .HasForeignKey(customer => customer.CustomerId);
            }
            );

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasMany(customer => customer.PromoCodes)
                .WithOne()
                .HasForeignKey(promocode => promocode.CustomerId);


                // entity.HasMany(customer => customer.CustomerPreferences)
                //.WithOne(cp=>cp.Customer)
                //.HasForeignKey(cp => cp.PreferenceId);

            });





            Seed(modelBuilder);
        }

        protected virtual void Seed(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Preference>().HasData(
        FakeDataFactory.Preferences
        );

            modelBuilder.Entity<Role>().HasData(
        FakeDataFactory.Roles
        );

            modelBuilder.Entity<Employee>().HasData(
        FakeDataFactory.Employees
        );



        }
    }
}
